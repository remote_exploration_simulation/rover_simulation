namespace rover_simulation.Scripts.Logging
{
    public interface ILogger
    {
        void SetLogLevel(LogLevel level);
        void LogDebug(string message);
        void LogInfo(string message);
        void LogWarn(string message);
        void LogError(string message);
        void LogFatal(string message);
    }
}