namespace rover_simulation.Scripts.Logging
{
    public enum LogLevel
    {
        Debug=0,
        Info=1,
        Warn=2,
        Error=3,
        Fatal=4
    }
}