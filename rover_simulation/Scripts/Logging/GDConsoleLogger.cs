using Godot;

namespace rover_simulation.Scripts.Logging
{
    public class GDConsoleLogger : ILogger
    {
        private LogLevel logLevel = LogLevel.Debug;
        
        public void SetLogLevel(LogLevel level)
        {
            logLevel = level;
        }

        public void LogDebug(string message)
        {
            if (logLevel.CompareTo(LogLevel.Debug) <= 0)
            {
                GD.Print("[DEBUG] " + message);
            }
        }

        public void LogInfo(string message)
        {
            if (logLevel.CompareTo(LogLevel.Info) <= 0)
            {
                GD.Print("[INFO]  " + message);
            }
        }

        public void LogWarn(string message)
        {
            if (logLevel.CompareTo(LogLevel.Warn) <= 0)
            {
                GD.Print("[WARN]  " + message);
            }
        }

        public void LogError(string message)
        {
            if (logLevel.CompareTo(LogLevel.Error) <= 0)
            {
                GD.PrintErr("[ERROR] " + message);
            }
        }

        public void LogFatal(string message)
        {
            if (logLevel >= LogLevel.Fatal)
            {
                GD.PrintErr("[FATAL] " + message);
            }
        }
    }
}