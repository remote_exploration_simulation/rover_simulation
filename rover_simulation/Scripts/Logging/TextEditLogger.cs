using System.Threading;
using Godot;
using rover_simulation.Scripts.Networking;

namespace rover_simulation.Scripts.Logging
{
    public class TextEditLogger: TextEdit, ILogger
    {
        public LogLevel logLevel { get; private set; }

        public override void _Init()
        {
            base._Init();
            logLevel = LogLevel.Debug;
        }

        public override void _Ready()
        {
            base._Ready();
             // Set highlighting
             AddColorRegion("[DEBUG]", "", Colors.White, true);
             AddColorRegion("[INFO ]", "", new Color("#87e673"), true);
             AddColorRegion("[WARN ]", "", new Color("#eaa656"), true);
             AddColorRegion("[ERROR]", "", new Color("#d86767"), true);
             AddColorRegion("[FATAL]", "", new Color("#d86767"), true);
             AddColorRegion("--", "--", Colors.HotPink, true);
             
             NetworkClient.Instance.SetLogger(this);
        }

        public void SetLogLevel(LogLevel level)
        {
            logLevel = level;
            string msg = "-- Set log level to ";
            switch (logLevel)
            {
                case LogLevel.Debug:
                    msg += "DEBUG"; break;
                case LogLevel.Info:
                    msg += "INFO"; break;
                case LogLevel.Warn:
                    msg += "WARN"; break;
                case LogLevel.Error:
                    msg += "ERROR"; break;
                case LogLevel.Fatal:
                    msg += "FATAL"; break;
            }

            WriteToTextEdit(msg + " --");
        }

        private void WriteToTextEdit(string message)
        {
            Text += message + "\n";
            CursorSetLine(GetLineCount());
        }

        public void LogDebug(string message)
        {
            if (logLevel.CompareTo(LogLevel.Debug) <= 0)
            {
                WriteToTextEdit("[DEBUG] " + message);                
            }
        }

        public void LogInfo(string message)
        {
            if (logLevel.CompareTo(LogLevel.Info) <= 0)
            {
                WriteToTextEdit("[INFO ] " + message);                
            }
        }

        public void LogWarn(string message)
        {
            if (logLevel.CompareTo(LogLevel.Warn) <= 0)
            {
                WriteToTextEdit("[WARN ] " + message);                
            }
        }

        public void LogError(string message)
        {
            if (logLevel.CompareTo(LogLevel.Error) <= 0)
            {
                WriteToTextEdit("[ERROR] " + message);                
            }
        }

        public void LogFatal(string message)
        {
            if (logLevel.CompareTo(LogLevel.Fatal) <= 0)
            {
                WriteToTextEdit("[FATAL] " + message);                
            }
        }
    }
}