namespace rover_simulation.Scripts.Networking
{
    public enum ConnectionStage
    {
        Disconnected, RequestSent, Connected
    }
}