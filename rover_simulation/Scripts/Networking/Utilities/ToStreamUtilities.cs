using System.IO;

namespace rover_simulation.Scripts.Networking.Utilities
{
    public static class ToStreamUtilities
    {
        public static Stream FileToInMemoryStream(string path, bool deleteFile = true)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }
            
            byte[] fileAsBytes = File.ReadAllBytes(path);
            Stream stream = new MemoryStream(fileAsBytes);

            if (deleteFile)
            {
                File.Delete(path);
            }

            return stream;
        }
    }
}