using System;

namespace rover_simulation.Scripts.Networking.Utilities
{
    public static class NetworkNumByteConverter
    {
        public static ushort ClientIdBytes2UShort(byte[] bytes, int start)
        {
            byte big = bytes[start];
            byte small = bytes[start + 1];

            return (ushort)((big << 8) | small);
        }

        public static byte[] ClientIdUShort2Bytes(ushort value)
        {
            int mask = 0b11111111;
            
            int big = value >> 8;
            int small = value & mask;

            return new[] { (byte)big, (byte)small };
        }

        public static uint SegmentBytes2UInt(byte[] bytes, int start)
        {
            byte big = bytes[start];
            byte medium = bytes[start + 1];
            byte small = bytes[start + 2];

            return (uint)((big << 16) | (medium << 8) | small);
        }

        public static byte[] SegmentUInt2Bytes(uint segment)
        {
            if (segment > int.MaxValue)
            {
                throw new OverflowException("Segment needs to be < " + int.MaxValue);
            }

            // Needed to perform binary operations
            int intSegment = (int)(segment);
            
            int mask = 0b11111111;
            
            int big = (intSegment >> 16) & mask;
            int medium = (intSegment >> 8) & mask;
            int small = intSegment & mask;

            return new[] { (byte)big, (byte)medium, (byte)small };
        }
    }
}