using System.Text;

namespace rover_simulation.Scripts.Networking.Utilities
{
    public static class ControlPackageUtilities
    {
        private static byte[] BuildControlMessage(byte type, byte[] ctrlMessage, ushort senderId, ushort receiverId = 0)
        {
            byte[] payload = new byte[ctrlMessage.Length + 1];
            payload[0] = type;
            for (int i = 1; i < payload.Length; i++)
            {
                payload[i] = ctrlMessage[i - 1];
            }
            
            var pkg = new SatelliteNetworkPackage(
                senderId,
                receiverId,
                true, 
                false,
                SenderType.Ground,
                PayloadType.Undefined,
                payload);

            return pkg.ToByteMessage();
        }

        public static byte[] BuildControlConnReq(string token)
        {
            byte[] tokenBytes = Encoding.UTF8.GetBytes(token);
            return BuildControlMessage((byte)2, tokenBytes, 0, 0);
        }
    }
}