using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Godot;
using rover_simulation.Scripts.Logging;
using rover_simulation.Scripts.Networking.Utilities;
using WebSocketSharp;
using LogLevel = rover_simulation.Scripts.Logging.LogLevel;

namespace rover_simulation.Scripts.Networking
{
    public class NetworkClient
    {
        public string url = "";
        private string token = "";

        private WebSocket ws;
        private ConnectionStage _connStage = ConnectionStage.Disconnected;

        private ConnectionStage ConnStage
        {
            get => _connStage;
            set
            {
                if (value != _connStage)
                {
                    _connStage = value;
                    foreach (var obs in observers)
                    {
                        obs.ObservedConnStateChanged(value);
                    }
                }
            }
        }

        public ushort id { get; private set; } 

        private readonly MessageBuffer messageBuffer = new MessageBuffer();
        private readonly MessageIdCreator idCreator = new MessageIdCreator();

        private readonly List<INetworkObserver> observers = new List<INetworkObserver>();

        private ILogger _logger;

        // Singleton pattern
        private static NetworkClient instance = null;
        public static NetworkClient Instance => instance ?? (instance = new NetworkClient());
        
        private NetworkClient()
        {
            Start();
        }

        public void Subscribe(INetworkObserver observer)
        {
            observers.Add(observer);
        }

        public void Unsubscribe(INetworkObserver observer)
        {
            observers.Remove(observer);
        }

        public void SetLogger(ILogger logger)
        {
            this._logger = logger;
        }

        public bool IsConnectedToNetwork => ConnStage == ConnectionStage.Connected;

        public void Connect(string ip, uint port, string token)
        {
            if (ConnStage == ConnectionStage.RequestSent)
            {
                _logger.LogWarn("Already in the process of connecting. Retry later.");
                return;
            }
            
            if (ConnStage == ConnectionStage.Connected)
            {
                string partOfToken = (token.Length > 5) ? token.Substring(0,  5) : token;
                _logger.LogWarn(
                    "Alrady connected to '"
                    + url
                    + "' with token name '"
                    + partOfToken);
                return;
            }
            
            url = "ws://" + ip + ":" + port.ToString();
            try
            {
                SetupWebSocket();
            }
            catch (ArgumentException e)
            {
                _logger.LogWarn("Invalid URI string '" + url + "'. Connecting to server was not attempted.");
                return;
            }
            
            _logger.LogInfo("Attempting connection to " + url);
            _logger.LogDebug("Attempting websocket connection to " + url);
            ws.Connect();

            this.token = token;
            _logger.LogDebug("Websocket connection established. Attempting handshake with server.");
            InitiateHandshake();
        }

        public void Disconnect()
        {
            ws.Close();
            ConnStage = ConnectionStage.Disconnected;
            string partOfToken = (token.Length > 5) ? token.Substring(0, 5) : token;
            string logMessage = "Disconnected from " + url + " using the token " + partOfToken;
            url = "";
            token = "";
            _logger.LogInfo(logMessage);
        }
        private void Start()
        {
            _logger = new GDConsoleLogger();
            _logger.SetLogLevel(LogLevel.Debug);
            
            System.Threading.Thread thread = new System.Threading.Thread(SendMessagesInQueue);
            thread.Start();
        }

        private void SetupWebSocket()
        {
            ws = new WebSocket(url);
            ws.OnMessage += (sender, e) => HandleMessage(e.RawData);
        }

        private void InitiateHandshake()
        {
            byte[] connReq = ControlPackageUtilities.BuildControlConnReq(token);
            ws.Send(connReq);
            ConnStage = ConnectionStage.RequestSent;
        }

        #region Receiving
        private void HandleMessage(IEnumerable<byte> message)
        {
            var package = new SatelliteNetworkPackage(message);

            if (package.isControl)
            {
                HandlePackageControl(package);
            }
            else
            {
                HandlePackageStd(package);
            }
        }

        private string MsgFromControlPkg(SatelliteNetworkPackage pkg)
        {
            byte[] text = new byte[pkg.Payload.Length - 1];
            for (int i = 1; i < pkg.Payload.Length; i++)
            {
                text[i - 1] = pkg.Payload[i];
            }
            return Encoding.UTF8.GetString(text);
        }

        private void HandlePackageControl(SatelliteNetworkPackage pkg)
        {
            int type = pkg.Payload[0];

            if (ConnStage == ConnectionStage.Disconnected)
            {
                string s = MsgFromControlPkg(pkg);
                _logger.LogDebug("Control package of type " + type + " with message: " + s + "\nDid nothing due to not being connected.");
            }
            else if (ConnStage == ConnectionStage.RequestSent)
            {
                // Only accept New Client ID / Conn Req. Accept or Connection refused
                // Connection refused
                if (type == 0)
                {
                    ConnStage = ConnectionStage.Disconnected;
                    string s = MsgFromControlPkg(pkg);
                    _logger.LogError("Connection was refused with message: '" + s + "'");
                }
                // Connection accepted / New Client ID
                else if (type == 1)
                {
                    id = NetworkNumByteConverter.ClientIdBytes2UShort(pkg.Payload, 1);
                    ConnStage = ConnectionStage.Connected;
                    _logger.LogInfo("Connected to server with id: " + id);
                    foreach (var observer in observers)
                    {
                        observer.ClientIdChanged(id);
                    }
                }
                else
                {
                    string s = MsgFromControlPkg(pkg);
                    _logger.LogDebug("Received unknown control package of type " + type + " with message: " + s);
                }
            }
            else if (ConnStage == ConnectionStage.Connected)
            {
                // Receive everything
                // Connection Refused
                if (type == 0)
                {
                    ConnStage = ConnectionStage.Disconnected;
                    string s = MsgFromControlPkg(pkg);
                    _logger.LogError("Connection was refused with message: '" + s + "'");
                }
                // New Client ID / Connection Request accepted
                else if (type == 1)
                {
                    id = BitConverter.ToUInt16(pkg.Payload, 1);
                    _logger.LogInfo("New ID from server: " + id);
                    foreach (var observer in observers)
                    {
                        observer.ClientIdChanged(id);
                    }
                }
                else
                {
                    string s = MsgFromControlPkg(pkg);
                    _logger.LogDebug("Received unknown control package of type " + type + " with message: " + s);
                }
            }
        }
        
        private void HandlePackageStd(SatelliteNetworkPackage pkg)
        {
            bool complete = messageBuffer.BufferSegment(pkg, _logger);

            if (!complete) return;

            Tuple<PayloadType, byte[]> buffered = messageBuffer.PopBufferedMessage(pkg.senderId, pkg.messageId);
            switch (buffered.Item1)
            {
                case PayloadType.Text:
                    string bufferStr = Encoding.UTF8.GetString(buffered.Item2);
                    ReceiveText(pkg.senderId, pkg.senderType, bufferStr);
                    break;
                case PayloadType.Image:
                    ReceiveImg(pkg.senderId, buffered.Item2);
                    break;
                case PayloadType.Undefined:
                    goto default;
                default:
                    ReceiveUndefined(pkg.senderId, buffered.Item2);
                    break;
            }
        }

        private void ReceiveText(ushort senderId, SenderType senderType, string text)
        {
            _logger.LogInfo("Received text: " + text);
            foreach (var observer in observers)
            {
                observer.ReceiveText(senderId, senderType, text);
            }
        }

        private void ReceiveImg(ushort senderId, byte[] imgAsBytes)
        {
            _logger.LogDebug("Received Image from " + senderId + ". Images can't be handled and get dropped.");    
        }

        private void ReceiveUndefined(ushort senderId, byte[] message)
        {
            _logger.LogDebug("Received Message of undefined type from " + senderId + ": " + message);    
        }
        #endregion

        #region Sending

        private struct StreamMessage
        {
            public PayloadType type;
            public Stream payload;
            public ushort receiverId;
        }

        private Queue<StreamMessage> streamMessageQueue = new Queue<StreamMessage>();

        private void SendMessagesInQueue()
        {
            while (true)
            {
                // And we are busy waiting. I know this isn't elegant but it works for now. If you, dear reader, have
                // the knowledge on how to properly wait on a new element being put into the queue, please hit me up.
                if (ConnStage != ConnectionStage.Connected) continue;
                if (streamMessageQueue.Count <= 0) continue;
                
                var streamMessage = streamMessageQueue.Dequeue();
                SendStream(streamMessage.payload, streamMessage.type, streamMessage.receiverId);
            }
        }

        public void SendText(string text, ushort receiverId = UInt16.MaxValue)
        {
            Stream textStream = new MemoryStream(Encoding.UTF8.GetBytes(text));
            var streamMessage = new StreamMessage
            {
                type = PayloadType.Text,
                payload = textStream,
                receiverId = receiverId
            };
            streamMessageQueue.Enqueue(streamMessage);
        }

        public void SendImage(Stream imageStream, ushort receiverId = UInt16.MaxValue)
        {
            var streamMessage = new StreamMessage
            {
                type = PayloadType.Image,
                payload = imageStream,
                receiverId = receiverId
            };
            streamMessageQueue.Enqueue(streamMessage);
        }

        private void SendStream(Stream stream, PayloadType payloadType, ushort receiverId)
        {
            byte[] bytes = new byte[SatelliteNetworkPackage.MAX_PAYLOAD_SIZE];
            int numBytesToBeRead = (int)stream.Length;
            byte messageId = idCreator.GetNextId();

            uint segment = 0;
            do
            {
                int n = stream.Read(bytes, 0, SatelliteNetworkPackage.MAX_PAYLOAD_SIZE);
                numBytesToBeRead -= n;

                byte[] payload = new byte[n];
                for (var i = 0; i < n; i++)
                {
                    payload[i] = bytes[i];
                }
                
                var pkg = new SatelliteNetworkPackage(
                    id,
                    receiverId,
                     false,
                    numBytesToBeRead == 0,
                    SenderType.Rover,
                    payloadType,
                    payload,
                    false,
                    messageId,
                    segment);

                // Busy waiting. Not perfect, but works for the purpose. Open for better ways to do this.
                while (ConnStage != ConnectionStage.Connected);
                
                ws.SendAsync(pkg.ToByteMessage(), null);
                segment++;
            } while (numBytesToBeRead > 0);
        }

        #endregion
    }
}