using System;
using System.Collections.Generic;
using rover_simulation.Scripts.Logging;

namespace rover_simulation.Scripts.Networking
{
    public class MessageBuffer
    {
        private class Buffer
        {
            private List<byte[]> segments;
            private int emptySegments = 0;
            private bool receivedLast = false;
            public PayloadType type { get; }

            public Buffer(PayloadType type)
            {
                segments = new List<byte[]>();
                emptySegments = 0;
                receivedLast = false;
                this.type = type;
            }

            public void AddSegment(uint segmentNumber, bool isLast, byte[] payload)
            {
                if (segments.Count < segmentNumber - 1)
                {
                    for (var i = 0; i < segmentNumber - segments.Count + 1; i++)
                    {
                        segments.Add(new byte[0]);
                        emptySegments++;
                    }
                }

                segments[(int)segmentNumber] = payload;
                emptySegments--;
                receivedLast = receivedLast || isLast;
            }

            public bool ReceivedAll => receivedLast && emptySegments == 0;

            public byte[] GetWholeMessage()
            {
                long totalLength = 0;
                foreach (var segment in segments)
                {
                    totalLength += segment.Length;
                }

                byte[] msg = new byte[totalLength];

                long msgCursor = 0;
                foreach (var segment in segments)
                {
                    for (var i = 0; i < segment.Length; i++)
                    {
                        msg[msgCursor] = segment[i];
                        msgCursor++;
                    }
                }

                return msg;
            }
        }

        private Dictionary<Tuple<ushort, byte>, Buffer> buffers;

        public MessageBuffer()
        {
            buffers = new Dictionary<Tuple<ushort, byte>, Buffer>();
        }

        public bool BufferSegment(SatelliteNetworkPackage package, ILogger logger = null)
        {
            var key = new Tuple<ushort, byte>(package.senderId, package.messageId);
            if (!buffers.ContainsKey(key))
            {
                buffers.Add(key, new Buffer(package.payloadType));
            }

            if (buffers[key].type != package.payloadType)
            {
                if (logger != null)
                {
                    logger.LogInfo("Received only partial package. Package got dropped.");
                }

                buffers[key] = new Buffer(package.payloadType);
            }
            
            buffers[key].AddSegment(package.segment, package.isLast, package.Payload);
            return buffers[key].ReceivedAll;
        }

        public Tuple<PayloadType, byte[]> PopBufferedMessage(ushort senderId, byte messageId)
        {
            var key = new Tuple<ushort, byte>(senderId, messageId);
            byte[] msg = buffers[key].GetWholeMessage();
            PayloadType type = buffers[key].type;
            buffers.Remove(key);
            return new Tuple<PayloadType, byte[]>(type, msg);
        }
    }
}