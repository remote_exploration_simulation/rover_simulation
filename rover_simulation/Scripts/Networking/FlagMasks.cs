namespace rover_simulation.Scripts.Networking
{
    public static class FlagMasks
    {
        public const byte CONTROL_FLAG = 0b1000_0000;
        public const byte LAST_FLAG = 0b0100_0000;
        public const byte SENDER_TYPE_FLAG = 0b0010_0000;
        public const byte TYPE_IMAGE_FLAG = 0b0001_0000;
        public const byte TYPE_TEXT_FLAG = 0b0000_1000;
    }
}