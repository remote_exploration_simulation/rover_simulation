namespace rover_simulation.Scripts.Networking
{
    public interface INetworkObserver
    {
        void ReceiveText(ushort senderId, SenderType senderType, string text);
        void ObservedConnStateChanged(ConnectionStage newConnectionState);
        void ClientIdChanged(uint newClientId);
    }
}