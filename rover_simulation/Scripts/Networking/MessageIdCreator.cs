using System;

namespace rover_simulation.Scripts.Networking
{
    public class MessageIdCreator
    {
        private byte id;

        public MessageIdCreator()
        {
            Random random = new Random();
            id = (byte)random.Next(0, 255);
        }

        public byte GetNextId()
        {
            byte old = id;
            id = (byte)((id + 1) % 255);
            return old;
        }
    }
}