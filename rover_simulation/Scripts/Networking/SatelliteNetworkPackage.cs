using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Godot;
using rover_simulation.Scripts.Networking.Exceptions;
using rover_simulation.Scripts.Networking.Utilities;

namespace rover_simulation.Scripts.Networking
{
    public class SatelliteNetworkPackage
    {
        public const int HEADER_SIZE = 5;
        public const int MAX_PACKAGE_SIZE = 4096;
        public const int MAX_PAYLOAD_SIZE = 4091;
        
        public ushort senderId { get; }
        public ushort receiverId { get; }
        public bool isControl { get; }
        public bool isLast { get; }
        public SenderType senderType { get; }
        public PayloadType payloadType { get; }
        public byte messageId { get; }
        public uint segment { get; }
        public bool isBroadcast { get; }
        
        private byte[] payload = new byte[0];

        public byte[] Payload
        {
            get => (byte[]) payload.Clone();
            private set => payload = value ?? new byte[0];
        }
        
        public SatelliteNetworkPackage(
            ushort senderId,
            ushort receiverId,
            bool isControl,
            bool isLast,
            SenderType senderType,
            PayloadType payloadType,
            byte[] payload,
            bool isBroadcast=false,
            byte messageId=0,
            uint segment=0)
        {
            this.senderId = senderId;
            this.receiverId = receiverId;
            this.isControl = isControl;
            this.isLast = isLast;
            this.senderType = senderType;
            this.payloadType = payloadType;
            this.messageId = messageId;
            this.segment = segment;

            if (payload.Count() > MAX_PAYLOAD_SIZE)
            {
                throw new SatelliteNetworkPackageTooLargeException();
            }
            
            this.Payload = payload;
            this.isBroadcast = isBroadcast;
        }
        
        public SatelliteNetworkPackage(IEnumerable<byte> message)
        {
            if (message is null)
                throw new NullReferenceException();

            byte[] byteMsg = message.ToArray();
            
            if (byteMsg.Length > MAX_PACKAGE_SIZE)
                throw new SatelliteNetworkPackageTooLargeException();

            if (byteMsg.Length < HEADER_SIZE)
                throw new ByteMessageTooSmallException();

            byte[] senderBytes = {byteMsg[0], byteMsg[1]};
            senderId = NetworkNumByteConverter.ClientIdBytes2UShort(senderBytes, 0);

            byte[] receiverBytes = {byteMsg[2], byteMsg[3]};
            receiverId = NetworkNumByteConverter.ClientIdBytes2UShort(receiverBytes, 0);

            isBroadcast = receiverId == 65535;

            byte flags = byteMsg[4];
            isControl = (flags & FlagMasks.CONTROL_FLAG) != 0;
            isLast = (flags & FlagMasks.LAST_FLAG) != 0;
            senderType = ((flags & FlagMasks.SENDER_TYPE_FLAG) != 0) ? SenderType.Rover : SenderType.Ground;
            
            if ((flags & FlagMasks.TYPE_IMAGE_FLAG) != 0)
            {
                payloadType = PayloadType.Image;
            }
            else if ((flags & FlagMasks.TYPE_TEXT_FLAG) != 0)
            {
                payloadType = PayloadType.Text;
            }
            else
            {
                payloadType = PayloadType.Undefined;
            }

            if (isControl)
            {
                messageId = 0;
                segment = 0;
                
                payload = new byte[byteMsg.Length - 5];
                for (int i = 5; i < byteMsg.Length; i++)
                {
                    payload[i - 5] = byteMsg[i];
                }
            }
            else
            {
                messageId = byteMsg[5];
                segment = NetworkNumByteConverter.SegmentBytes2UInt(byteMsg, 6);
                
                payload = new byte[byteMsg.Length - 9];
                for (int i = 9; i < byteMsg.Length; i++)
                {
                    payload[i - 9] = byteMsg[i];
                }
            }
        }

        public byte[] ToByteMessage()
        {
            byte[] senderBytes = NetworkNumByteConverter.ClientIdUShort2Bytes(senderId);
            byte[] receiverBytes = NetworkNumByteConverter.ClientIdUShort2Bytes((ushort)((isBroadcast) ? 65535 : receiverId));

            byte flags = 0;
            flags |= isControl ? FlagMasks.CONTROL_FLAG : (byte) 0;
            flags |= isLast ? FlagMasks.LAST_FLAG : (byte) 0;
            flags |= (senderType == SenderType.Rover) ? FlagMasks.SENDER_TYPE_FLAG : (byte) 0;
            flags |= (payloadType == PayloadType.Image) ? FlagMasks.TYPE_IMAGE_FLAG : (byte) 0;
            flags |= (payloadType == PayloadType.Text) ? FlagMasks.TYPE_TEXT_FLAG : (byte) 0;

            byte[] segmentBytes = NetworkNumByteConverter.SegmentUInt2Bytes(segment);
            
            byte[] msg = new byte[payload.Length + (isControl ? 5 : 9)];
            
            msg[0] = senderBytes[0];
            msg[1] = senderBytes[1];
            msg[2] = receiverBytes[0];
            msg[3] = receiverBytes[1];
            msg[4] = flags;
            
            if (!isControl)
            {
                msg[5] = messageId;
                msg[6] = segmentBytes[0];
                msg[7] = segmentBytes[1];
                msg[8] = segmentBytes[2];
            }

            for (int i = 0; i < payload.Length; i++)
            {
                msg[i + (isControl ? 5 : 9)] = payload[i];
            }

            return msg;
        }

        public override string ToString()
        {
            string s = "SatelliteNetworkPackage\n";
            s += "sender: " + senderId.ToString();
            s += " (" + ((senderType == SenderType.Ground) ? "ground" : "rover") + ")";
            s += " --> receiver: " + receiverId.ToString();
            s += isBroadcast ? " (broadcast)\n" : "\n";
            
            if (isControl)
            {
                s += "Control Package\n";
            }
            else if (payloadType == PayloadType.Image)
            {
                s += "Image Package\n";
            }
            else if (payloadType == PayloadType.Text)
            {
                s += "Text Package\n";
            }
            else
            {
                s += "Undefined Package\n";
            }

            s += "message ID: " + messageId + "\n";
            s += "segment:    " + segment + "\n";
            s += "last:       " + isLast + "\n";
            s += "payload:\n";

            if (isControl)
            {
                byte[] code = {payload[0]};
                s += payload[0].ToString() + " - ";

                byte[] text = new byte[payload.Length - 1];
                for (int i = 1; i < payload.Length; i++)
                {
                    text[i - 1] = payload[i];
                }
                
                s += Encoding.UTF8.GetString(text);
            }
            else if (payloadType == PayloadType.Text)
            {
                s += Encoding.UTF8.GetString(payload);
            }
            else
            {
                foreach (var b in payload)
                {
                    s += b + " ";
                }
            }

            return "-----------------------------------------\n" + s + "\n-----------------------------------------";
        }
    }
}