using System.Collections.Generic;
using System.Linq;

namespace rover_simulation.Scripts.Networking
{
    public class PayloadBuffer
    {
        public PayloadType type { get; private set; }
        private List<byte> buffer = new List<byte>();

        public PayloadBuffer(PayloadType type, IEnumerable<byte> payloadBytes = null)
        {
            this.type = type;

            if (payloadBytes != null)
            {
                Append(payloadBytes);
            }
        }

        public void Append(IEnumerable<byte> payloadBytes)
        {
            foreach (byte b in payloadBytes)
            {
                buffer.Add(b);
            }
        }

        public byte[] GetBuffer()
        {
            return buffer.Count == 0 ? new byte[0] : buffer.ToArray();
        }
    }
}