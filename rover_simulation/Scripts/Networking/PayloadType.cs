namespace rover_simulation.Scripts.Networking
{
    public enum PayloadType
    {
        Undefined, Text, Image
    }
}