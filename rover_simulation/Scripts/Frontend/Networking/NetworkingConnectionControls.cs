using System;
using System.Linq;
using Godot;
using rover_simulation.Scripts.Networking;

namespace rover_simulation.Scripts.Frontend.Networking
{
    public class NetworkingConnectionControls: Node, INetworkObserver
    {
        // Panels
        private Control connectedPanel;
        private Control connectingPanel;
        private Control disconnectedPanel;
        
        // Connected Components
        private Label connectedLabelIP;
        private Label connectedLabelPort;
        private Label connectedLabelToken;
        private Label connectedLabelClientID;
        private Button connectedButtonDisconnect;
        
        // Connecting Components
        private Button connectingButtonStop;
        
        // Disconnected Components
        private LineEdit disconnectedLineEditIP;
        private SpinBox disconnectedSpinBoxPort;
        private LineEdit disconnectedLineEditToken;
        private Button disconnectedButtonConnect;

        // Values stored when the "Connect" button is clicked
        private string ipSetOnConnect = "";
        private uint portSetOnConnect = 0;
        private string tokenNameSetOnConnect = "";

        
        public override void _Ready()
        {
            NetworkClient.Instance.Subscribe(this);
            
            connectedPanel = GetNode<Control>("Connected");
            connectingPanel = GetNode<Control>("Connecting");
            disconnectedPanel = GetNode<Control>("Disconnected");

            connectedLabelIP = GetNode<Label>("Connected/Info/IP");
            connectedLabelPort = GetNode<Label>("Connected/Info/Port");
            connectedLabelToken = GetNode<Label>("Connected/Info/Token");
            connectedLabelClientID = GetNode<Label>("Connected/Info/ClientID");
            connectedButtonDisconnect = GetNode<Button>("Connected/ButtonDisconnect");

            connectingButtonStop = GetNode<Button>("Connecting/ButtonStop");

            disconnectedLineEditIP = GetNode<LineEdit>("Disconnected/IP");
            disconnectedSpinBoxPort = GetNode<SpinBox>("Disconnected/Port");
            disconnectedLineEditToken = GetNode<LineEdit>("Disconnected/Token");
            disconnectedButtonConnect = GetNode<Button>("Disconnected/ButtonConnect");

            connectedButtonDisconnect.Connect(
                "pressed", 
                this, 
                nameof(DisconnectNetworkClient));
            connectingButtonStop.Connect(
                "pressed",
                this,
                nameof(DisconnectNetworkClient));
            disconnectedButtonConnect.Connect(
                "pressed",
                this,
                nameof(ConnectNetworkClient));
        }

        public void ReceiveText(ushort senderId, SenderType senderType, string text)
        {
            // Do nothing.
        }

        public void ObservedConnStateChanged(ConnectionStage newConnectionState)
        {
            SetPanel(newConnectionState);
        }

        public void ClientIdChanged(uint newClientId)
        {
            RefreshConnectedPanelContents();
        }

        private void SetPanel(ConnectionStage connectionStage)
        {
            connectedPanel.Visible = (connectionStage == ConnectionStage.Connected);
            connectingPanel.Visible = (connectionStage == ConnectionStage.RequestSent);
            disconnectedPanel.Visible = (connectionStage == ConnectionStage.Disconnected);
        }

        private void RefreshConnectedPanelContents()
        {
            connectedLabelIP.Text = ipSetOnConnect;
            connectedLabelPort.Text = portSetOnConnect.ToString();
            connectedLabelToken.Text = tokenNameSetOnConnect;
            connectedLabelClientID.Text = NetworkClient.Instance.id.ToString();
        }

        private void ConnectNetworkClient()
        {
            string ip = ipSetOnConnect = disconnectedLineEditIP.Text;
            
            string token = disconnectedLineEditToken.Text;
            tokenNameSetOnConnect = token.Substring(0, 5);
            
            uint port = portSetOnConnect = Convert.ToUInt32(disconnectedSpinBoxPort.Value);
            
            NetworkClient.Instance.Connect(ip, port, token);
            RefreshConnectedPanelContents();
        }

        private void DisconnectNetworkClient()
        {
            NetworkClient.Instance.Disconnect();
        }
    }
}