using Godot;
using System;
using rover_simulation.Scripts.Logging;
using Object = Godot.Object;

public class NetworkingLoggingControls : Node
{
    private TextEditLogger logger;
    private Slider logLevelSlider;

    public override void _Ready()
    {
        logger = GetNode<TextEditLogger>("LoggingOutputMargin/LoggingOutput");
        logLevelSlider = GetNode<Slider>("LoggingSettingsMargin/LoggingSettings/LogLevelSlider");
        logLevelSlider.Connect("value_changed", this, nameof(UpdateLogLevelThroughSlider));
        
        UpdateLogLevelThroughSlider(new Object());
    }

    private void UpdateLogLevelThroughSlider(Object obj)
    {
        int logLevelClamped = (int)Math.Max(0, Math.Min(logLevelSlider.Value, 4));
        int logLevelNumeric = 4 - logLevelClamped;
        logger.SetLogLevel((LogLevel)logLevelNumeric);
    }
}
